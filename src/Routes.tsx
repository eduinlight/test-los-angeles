import React from 'react';
import {Route} from 'react-router-native';
import Stack from 'react-router-native-stack';
// components
import ProductsListRouter from './routes/ProductsListRouter';
import ProductFormRouter from './routes/ProductFormRouter';

// TODO: separate this on another file to avoid circle requires
export enum Paths {
  PRODUCTS_LIST = '/',
  PRODUCT_FORM = '/form',
}

const routes = [
  {
    path: Paths.PRODUCTS_LIST,
    exact: true,
    component: ProductsListRouter,
  },
  {
    path: Paths.PRODUCT_FORM,
    exact: true,
    component: ProductFormRouter,
  },
];

const Routes = () => {
  return (
    <Stack>
      {routes.map((route) => {
        return (
          <Route
            key={route.path}
            path={route.path}
            exact={route.exact}
            render={(props: any) => <route.component {...props} />}
          />
        );
      })}
    </Stack>
  );
};

export default Routes;

import baseConfig from './base-config';
import development from './development';
import test from './testing';
import production from './production';
import deepmerge from 'deepmerge';
import {ConfigType} from './base-config';

type EnvType = 'development' | 'production' | 'test';

let ENV: EnvType = 'development';
if (
  process.env.NODE_ENV === 'production' ||
  process.env.NODE_ENV === 'development' ||
  process.env.NODE_ENV === 'test'
) {
  ENV = process.env.NODE_ENV;
}

const configEnviroment = {
  development,
  production,
  test,
};

const Config: ConfigType = deepmerge.all([
  baseConfig,
  configEnviroment[ENV],
]) as ConfigType;

export default Config;

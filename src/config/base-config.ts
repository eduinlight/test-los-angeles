const baseConfig = {
  apiurl: 'http://172.20.10.2:3000',
};

export default baseConfig;

export type ConfigType = typeof baseConfig;

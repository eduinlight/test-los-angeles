import {NativeBase} from 'native-base';

class ThemeClass {
  colors = {
    primary: '#1d1d1d',
    secondary: '#31419F',
    background: '#1d1d1d',
    placeholder: '#1d1d1d',
    border: '#8c8c8c',
    grey0: '#eee',
    error: '#ED2E2E',
    white: '#fefefe',
    black: '#1d1d1d',
    lightBlack: '#3b3a3a',
    label: '#262626',
  };
  spacing = (value: number) => value * 8;
  sizes = {
    btnHeight: 50,
    headerHeight: 80,
  };

  icons: Record<string, NativeBase.Icon> = {
    arrowUp: {
      name: 'arrow-up',
      type: 'MaterialCommunityIcons',
    },
    plus: {
      name: 'pluscircle',
      type: 'AntDesign',
    },
    info: {
      name: 'information',
      type: 'MaterialCommunityIcons',
    },
    delete: {
      name: 'delete',
      type: 'MaterialCommunityIcons',
    },
    edit: {
      name: 'edit',
      type: 'MaterialIcons',
    },
    close: {
      name: 'close',
      type: 'MaterialCommunityIcons',
    },
    search: {
      name: 'search',
      type: 'MaterialIcons',
    },
    backIcon: {
      name: 'chevron-left',
      type: 'MaterialIcons',
    },
    dolar: {
      name: 'attach-money',
      type: 'MaterialIcons',
    },
    error: {
      name: 'close-circle',
      type: 'MaterialCommunityIcons',
    },
    success: {
      name: 'check-circle',
      type: 'MaterialIcons',
    },
  };
  textIcons = {
    fingerDown: '👇',
  };
}

const Theme = new ThemeClass();

export default Theme;

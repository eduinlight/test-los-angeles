import React from 'react';
import ProductForm from '../components/ProductForm';
import FormLayout from '../components/layouts/FormLayout';
import useTranslate from '../core/hooks/translate/useTranslate';
import {useLocation} from 'react-router-native';
import {useTypedSelector} from '../core/hooks/use_typed_selector';

const ProductFormRouter = () => {
  const {translate} = useTranslate();
  const {
    state: {type},
  } = useLocation();
  const selected = useTypedSelector((store) => store.products.selected);

  return (
    <FormLayout
      title={translate(
        type === 'add' ? 'addProductTitle' : 'editProductTitle',
      )}>
      <ProductForm type={type} selectedProduct={selected} />
    </FormLayout>
  );
};

export default ProductFormRouter;

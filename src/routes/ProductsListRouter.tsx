import React from 'react';
import ProductsList from '../components/ProductsList';

const ProductsListRouter = () => {
  return <ProductsList />;
};

export default ProductsListRouter;

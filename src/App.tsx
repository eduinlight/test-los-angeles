import React from 'react';
import {NativeRouter, BackButton} from 'react-router-native';
import {Provider} from 'react-redux';
import {store} from './redux/store';
import Routes from './Routes';
import {StyleProvider, Root} from 'native-base';
import {SafeAreaView, StyleSheet} from 'react-native';
import getTheme from '../native-base-theme/components';
import custom from '../native-base-theme/variables/custom';
import SplashScreen from 'react-native-splash-screen';
import LoadApp from './LoadApp';
import useOnMount from './core/hooks/useOnMount';

// this is for disable warnings
console.disableYellowBox = true;

const App = () => {
  useOnMount(() => {
    SplashScreen.hide();
  });

  return (
    <Provider store={store}>
      <LoadApp>
        <SafeAreaView style={styles.safeArea}>
          <Root>
            <StyleProvider style={getTheme(custom)}>
              <NativeRouter>
                <BackButton>
                  <Routes />
                </BackButton>
              </NativeRouter>
            </StyleProvider>
          </Root>
        </SafeAreaView>
      </LoadApp>
    </Provider>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
});

export default App;

import Product from '../../core/interfases/product';

export const PRODUCTS_SET = 'PRODUCTS_SET';
export const PRODUCTS_UPDATE = 'PRODUCTS_UPDATE';
export const PRODUCTS_ADD = 'PRODUCTS_ADD';
export const PRODUCTS_DELETE = 'PRODUCTS_DELETE';
export const PRODUCTS_SET_SELECTED = 'PRODUCTS_SET_SELECTED';

export type AppStateType = 'active' | 'background';

export type Language = 'es' | 'en';

export interface ProductsState {
  products: Product[];
  selected: Product | null;
}

export interface SetProducts {
  type: typeof PRODUCTS_SET;
  payload: Product[];
}

export interface UpdateProduct {
  type: typeof PRODUCTS_UPDATE;
  payload: {id: Product['id']; product: Product};
}

export interface AddProduct {
  type: typeof PRODUCTS_ADD;
  payload: Product;
}

export interface DeleteProducts {
  type: typeof PRODUCTS_DELETE;
  payload: Product['id'][];
}

export interface SetSelected {
  type: typeof PRODUCTS_SET_SELECTED;
  payload: Product;
}

export type ProductsActionType =
  | SetProducts
  | UpdateProduct
  | DeleteProducts
  | AddProduct
  | SetSelected;

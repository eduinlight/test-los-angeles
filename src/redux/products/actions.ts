import {
  SetProducts,
  PRODUCTS_SET,
  PRODUCTS_ADD,
  UpdateProduct,
  AddProduct,
  PRODUCTS_UPDATE,
  PRODUCTS_DELETE,
  DeleteProducts,
  SetSelected,
  PRODUCTS_SET_SELECTED,
} from './types';
import Product from '../../core/interfases/product';

class ProductsActionsClass {
  setSelected(product: Product): SetSelected {
    return {
      type: PRODUCTS_SET_SELECTED,
      payload: product,
    };
  }

  setProducts(products: Product[]): SetProducts {
    return {
      type: PRODUCTS_SET,
      payload: products,
    };
  }

  addProduct(product: Product): AddProduct {
    return {
      type: PRODUCTS_ADD,
      payload: product,
    };
  }

  updateProduct(id: Product['id'], product: Product): UpdateProduct {
    return {
      type: PRODUCTS_UPDATE,
      payload: {id, product},
    };
  }

  deleteProduct(ids: Product['id'][]): DeleteProducts {
    return {
      type: PRODUCTS_DELETE,
      payload: ids,
    };
  }
}

const ProductsActions = new ProductsActionsClass();

export default ProductsActions;

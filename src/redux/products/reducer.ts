import produce from 'immer';
import {
  PRODUCTS_SET,
  PRODUCTS_ADD,
  PRODUCTS_DELETE,
  PRODUCTS_UPDATE,
  ProductsState,
  ProductsActionType,
  PRODUCTS_SET_SELECTED,
} from './types';

const ini = {
  products: [],
  selected: null,
} as ProductsState;

// this is an example reducer using immer
export default (oldState = ini, action: ProductsActionType) =>
  produce(oldState, (state: ProductsState) => {
    switch (action.type) {
      case PRODUCTS_SET_SELECTED: {
        state.selected = action.payload;
        break;
      }
      case PRODUCTS_SET: {
        state.products = action.payload;
        break;
      }
      case PRODUCTS_ADD: {
        state.products = [action.payload, ...state.products];
        break;
      }
      case PRODUCTS_UPDATE: {
        for (let i = 0; i < state.products.length; i++) {
          const product = state.products[i];
          if (product.id === action.payload.id) {
            state.products[i] = action.payload.product;
          }
        }
        break;
      }
      case PRODUCTS_DELETE: {
        state.products = state.products.filter(
          (product) => !action.payload.includes(product.id),
        );
      }
    }
  });

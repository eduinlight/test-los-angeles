import {createStore, applyMiddleware} from 'redux';
import rootReducer from './rootReducer';

const initialState = {};

const middlewares = applyMiddleware();

export const store = createStore(rootReducer, initialState, middlewares);

import {combineReducers} from 'redux';
import app from './app/reducer';
import products from './products/reducer';

const rootReducer = combineReducers({
  app,
  products,
});

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;

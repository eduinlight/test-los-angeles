export const APP_SET_STATE = 'APP_SET_STATE';
export const APP_SET_FIRST_START = 'APP_SET_FIRST_START';
export const APP_SET_LANGUAGE = 'APP_SET_LANGUAGE';

export type AppStateType = 'active' | 'background';

export type Language = 'es' | 'en';

export interface AppState {
  state: AppStateType;
  firstStart: boolean;
  language: Language;
}

export interface AppSetLanguage {
  type: typeof APP_SET_LANGUAGE;
  payload: Language;
}

export interface AppSetStateAction {
  type: typeof APP_SET_STATE;
  payload: AppStateType;
}

export interface AppSetFirstStart {
  type: typeof APP_SET_FIRST_START;
  payload: boolean;
}

export type AppActionType =
  | AppSetStateAction
  | AppSetFirstStart
  | AppSetLanguage;

import {
  AppSetStateAction,
  APP_SET_STATE,
  APP_SET_FIRST_START,
  AppSetFirstStart,
  AppStateType,
  Language,
  AppSetLanguage,
  APP_SET_LANGUAGE,
} from './types';

class AppActionsClass {
  setLanguaje(language: Language): AppSetLanguage {
    return {
      type: APP_SET_LANGUAGE,
      payload: language,
    };
  }

  setAppState(state: AppStateType): AppSetStateAction {
    return {
      type: APP_SET_STATE,
      payload: state,
    };
  }

  setFirstStart(value = false): AppSetFirstStart {
    return {
      type: APP_SET_FIRST_START,
      payload: value,
    };
  }
}

const AppActions = new AppActionsClass();

export default AppActions;

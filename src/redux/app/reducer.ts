import produce from 'immer';
import {
  APP_SET_STATE,
  APP_SET_FIRST_START,
  APP_SET_LANGUAGE,
  AppState,
  AppActionType,
} from './types';

export const APP_STATE_FIRST_START = 'app-state-first-start';
export const APP_STATE_LANGUAGE = 'app-state-language';

const ini = {
  state: 'background',
  firstStart: true,
  language: 'en',
} as AppState;

// this is an example reducer using immer
export default (oldState = ini, action: AppActionType) =>
  produce(oldState, (state: AppState) => {
    switch (action.type) {
      case APP_SET_LANGUAGE: {
        state.language = action.payload;
        // Storage.save(APP_STATE_LANGUAGE, action.payload);
        break;
      }
      case APP_SET_STATE: {
        state.state = action.payload;
        break;
      }
      case APP_SET_FIRST_START: {
        state.firstStart = action.payload;
        // Storage.save(APP_STATE_FIRST_START, action.payload ? 'true' : 'false');
        break;
      }
    }
  });

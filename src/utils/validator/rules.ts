// THIS SCRIPT IT WAS CREATED BY EDUIN GARCIA CORDERO
// IS A DATA VALIDATOR LIBRARY USING THE FUNCTIONS PROVIDED BY string-validator
// YOU CAN USE THIS CODE AND DO WHEREVER YOU WANTED WITH IT
import validator from 'validator';

export default {
  equals: validator.equals,
  contains: (value: string, regex: RegExp): boolean => regex.test(value),
  matches: validator.matches,
  email: validator.isEmail,
  url: validator.isURL,
  ip: validator.isIP,
  alpha: validator.isAlpha,
  numeric: validator.isNumeric,
  alphaNumeric: validator.isAlphanumeric,
  base64: validator.isBase64,
  hexadecimal: validator.isHexadecimal,
  hexcolor: validator.isHexColor,
  lowercase: validator.isLowercase,
  uppercase: validator.isUppercase,
  int: validator.isInt,
  float: validator.isFloat,
  divisibleBy: validator.isDivisibleBy,
  required: (value: string): boolean =>
    value !== undefined && value !== null && value !== '',
  minLength: (value: string, length: number): boolean => value.length >= length,
  maxLength: (value: string, length: number): boolean => value.length <= length,
  date: validator.isISO8601,
  afterDate: validator.isAfter,
  beforeDate: validator.isBefore,
  in: validator.isIn,
  creditCard: validator.isCreditCard,
  json: validator.isJSON,
  ascii: validator.isAscii,
};

// THIS SCRIPT IT WAS CREATED BY EDUIN GARCIA CORDERO
// IS A DATA VALIDATOR LIBRARY USING THE FUNCTIONS PROVIDED BY string-validator
// YOU CAN USE THIS CODE AND DO WHEREVER YOU WANTED WITH IT

import rules from './rules';

export type RuleType = keyof typeof rules;

export type DataType = Record<string, any>;

export type RuleParamType =
  | string
  | number
  | Date
  | Record<string, unknown>
  | RegExp;

export interface IObjectRuleType {
  rule: RuleType;
  params?: RuleParamType[];
  message?: string;
}

export type SchemaType = Record<string, Array<RuleType | IObjectRuleType>>;

export class FieldError {
  constructor(public field: string, public error: string) {}
}

export interface IValidationResponse {
  valid: boolean;
  errors: FieldError[];
  values: DataType;
}

export interface IRuleFunction {
  (value: RuleParamType, ...params: Array<RuleParamType>): boolean;
}

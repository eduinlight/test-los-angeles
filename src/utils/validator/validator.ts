// THIS SCRIPT IT WAS CREATED BY EDUIN GARCIA CORDERO
// IS A DATA VALIDATOR LIBRARY USING THE FUNCTIONS PROVIDED BY string-validator
// YOU CAN USE THIS CODE AND DO WHEREVER YOU WANTED WITH IT

import {
  DataType,
  RuleType,
  SchemaType,
  IValidationResponse,
  FieldError,
  RuleParamType,
  IRuleFunction,
} from './types';
import locals from './locals';
import rules from './rules';

const defaultLocales = locals.en;

class PopulatedRule {
  rule: IRuleFunction = () => true;
  params: RuleParamType[] = [];
  message = '';
}

class ValidatorClass {
  private defaultLocales = defaultLocales;

  public setLocales(locales: Record<RuleType, string>): void {
    this.defaultLocales = locales;
  }

  public validate(form: DataType, schema: SchemaType): IValidationResponse {
    // initialize response object
    const res = {
      errors: [],
      valid: true,
      values: {...form},
    } as IValidationResponse;
    // for every schema field
    Object.keys(schema).forEach((field) => {
      // get rules of the field
      const fieldRules = schema[field];

      // value of the field to answare
      res.values[field] = form[field];

      let hasError = false;
      let required = false;

      // for every rule of the field
      fieldRules.forEach((rule) => {
        if (hasError) {
          return;
        }

        // define the rule
        const populatedRule = new PopulatedRule();
        if (typeof rule === 'string') {
          if (rule === 'required') {
            required = true;
          }
          populatedRule.rule = rules[rule] as any;
          populatedRule.message = this.defaultLocales[rule];
          populatedRule.params = [];
        } else if (typeof rule === 'object') {
          if (rule.rule === 'required') {
            required = true;
          }
          populatedRule.rule = rules[rule.rule] as any;
          populatedRule.message = this.defaultLocales[rule.rule];
          populatedRule.params = [];

          if (
            populatedRule.rule.length > 0 &&
            rule.params &&
            rule.params.length > 0
          ) {
            populatedRule.params = rule.params;
          }

          if (rule.message) {
            populatedRule.message = rule.message;
          }
        }
        if (
          !populatedRule.rule(
            form[field] !== undefined ? form[field].toString() : '',
            ...populatedRule.params,
          )
        ) {
          res.valid = false;
          // replace params on message
          populatedRule.params.forEach((param: any, i: any) => {
            populatedRule.message = populatedRule.message.replace(
              new RegExp('%p' + (i + 1), 'g'),
              JSON.stringify(param),
            );
          });
          res.errors.push(new FieldError(field, populatedRule.message));
          hasError = true;
        }
      });

      if (hasError && !required && !rules.required(form[field])) {
        res.errors.pop();

        if (res.errors.length === 0) {
          res.valid = true;
        }
      }
    });
    return res;
  }
}

const Validator = new ValidatorClass();

export default Validator;

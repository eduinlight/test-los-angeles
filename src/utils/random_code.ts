class RandomCodeClass {
  generate(length: number): string {
    let i = length;
    const code = [];
    while (i > 0) {
      i--;
      code.push(Math.ceil(Math.random() * 10) % 10);
    }
    return code.join('');
  }

  random(a: number, b: number) {
    return a + Math.floor((Math.random() * 100000) % (b + 1));
  }
}

const RandomCode = new RandomCodeClass();

export default RandomCode;

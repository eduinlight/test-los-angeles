import React, {FunctionComponent} from 'react';
import Loading from './components/shared/Loading';

interface Props {}

// this component is usefull for prepare app before start
const LoadApp: FunctionComponent<React.PropsWithChildren<Props>> = ({
  children,
}) => {
  const loading = false;

  if (loading) {
    return <Loading />;
  }

  return <>{children}</>;
};

export default LoadApp;

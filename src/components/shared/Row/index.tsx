import React, {FunctionComponent, Children, isValidElement} from 'react';
import {View, NativeBase} from 'native-base';
import {StyleSheet, ViewStyle} from 'react-native';

export interface RowProps extends NativeBase.View {
  spacing?: number;
  reverse?: boolean;
  justifyContent?: ViewStyle['justifyContent'];
  flex?: ViewStyle['flex'];
  alignItems?: ViewStyle['alignItems'];
}

const Row: FunctionComponent<React.PropsWithChildren<RowProps>> = ({
  children,
  spacing = 0,
  reverse = false,
  style = {},
  flex = 1,
  justifyContent = 'flex-start',
  alignItems = 'stretch',
  ...props
}) => {
  const childrensCount = Children.count(children);

  const cildrenWithSpace = Children.map(children, (child, index) => {
    const spacingCalc = index === childrensCount - 1 ? 0 : spacing;

    const spacingComponent = spacingCalc > 0 && (
      <View style={{width: spacingCalc}} />
    );

    if (isValidElement(child)) {
      return (
        <>
          {child}
          {spacingComponent}
        </>
      );
    }

    return child;
  });

  const rowType = reverse ? styles.rowReverse : styles.row;

  return (
    <View
      style={{
        ...rowType,
        justifyContent,
        flex,
        alignItems,
        ...(style as any),
      }}
      {...props}>
      {cildrenWithSpace}
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  rowReverse: {
    flexDirection: 'row-reverse',
  },
});

export default Row;

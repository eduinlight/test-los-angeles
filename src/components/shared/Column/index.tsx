import React, {FunctionComponent, Children, isValidElement} from 'react';
import {View, NativeBase} from 'native-base';
import {StyleSheet, ViewStyle} from 'react-native';

export interface ColumnProps extends NativeBase.View {
  spacing?: number;
  reverse?: boolean;
  justifyContent?: ViewStyle['justifyContent'];
  alignItems?: ViewStyle['alignItems'];
  flex?: ViewStyle['flex'];
}

const Column: FunctionComponent<React.PropsWithChildren<ColumnProps>> = ({
  children,
  spacing = 0,
  reverse = false,
  style = {},
  justifyContent = 'flex-start',
  alignItems = 'stretch',
  flex = 1,
  ...props
}) => {
  const childrensCount = Children.count(children);

  const cildrenWithSpace = Children.map(children, (child, index) => {
    const spacingCalc = index === childrensCount - 1 ? 0 : spacing;

    const spacingComponent = spacingCalc > 0 && (
      <View style={{height: spacingCalc}} />
    );

    if (isValidElement(child)) {
      return (
        <>
          {child}
          {spacingComponent}
        </>
      );
    }

    return child;
  });

  const columnType = reverse ? styles.columnReverse : styles.column;

  return (
    <View
      style={{
        ...columnType,
        justifyContent,
        alignItems,
        flex,
        ...(style as any),
      }}
      {...props}>
      {cildrenWithSpace}
    </View>
  );
};

const styles = StyleSheet.create({
  column: {
    flexDirection: 'column',
  },
  columnReverse: {
    flexDirection: 'column-reverse',
  },
});

export default Column;

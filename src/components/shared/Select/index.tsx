import React, {PropsWithChildren} from 'react';
import {Icon, Item, Label, Picker, NativeBase} from 'native-base';
import {StyleSheet} from 'react-native';
import Theme from '../../../theme';

export interface SelectProps extends NativeBase.Picker {
  itemProps?: NativeBase.Item;
  label?: string;
  labelProps?: NativeBase.Label;
}

export const SelectOption = Picker.Item;

const Select: PropsWithChildren<React.FC<SelectProps>> = ({
  children,
  itemProps = {},
  label = '',
  labelProps = {},
  ...props
}) => {
  return (
    <>
      <Label style={styles.label} {...labelProps}>
        {label}
      </Label>
      <Item picker {...itemProps}>
        <Picker
          mode="dropdown"
          iosIcon={<Icon style={styles.colorWhite} name="arrow-down" />}
          style={{width: undefined, ...styles.colorWhite}}
          placeholderStyle={styles.colorWhite}
          placeholderIconColor={Theme.colors.white}
          {...props}>
          {children}
        </Picker>
      </Item>
    </>
  );
};

const styles = StyleSheet.create({
  label: {
    color: Theme.colors.white,
    opacity: 0.7,
    fontSize: 14,
  },
  colorWhite: {
    color: Theme.colors.white,
  },
});

export default Select;

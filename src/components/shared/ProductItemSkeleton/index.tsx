import React, {useMemo} from 'react';
import {StyleSheet} from 'react-native';
import Theme from '../../../theme';
import Collumn from '../Column';
import Row from '../Row';
import Skeleton from '../Skeleton';

export interface ProductItemSkeletonProps {
  count?: number;
}

const ProductItemSkeleton: React.FC<ProductItemSkeletonProps> = ({
  count = 1,
}) => {
  const keys = useMemo(() => {
    const res = [];
    for (let i = 0; i < count; i++) {
      res.push('key_' + i);
    }
    return res;
  }, [count]);

  return (
    <>
      {keys.map((key) => (
        <Row key={key} flex={0} style={styles.container}>
          <Collumn flex={1} justifyContent="space-evenly">
            <Skeleton width={100} height={15} borderRadius={10} />
            <Skeleton width={200} height={10} borderRadius={10} />
          </Collumn>
          <Collumn justifyContent="center" alignItems="flex-end" flex={1}>
            <Skeleton width={50} height={15} borderRadius={10} />
          </Collumn>
        </Row>
      ))}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Theme.colors.background,
    padding: Theme.spacing(1),
    height: 70,
    borderBottomColor: Theme.colors.border,
    borderBottomWidth: 1,
  },
});

export default ProductItemSkeleton;

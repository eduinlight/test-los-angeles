import React, {FunctionComponent} from 'react';
import {Icon, Button as NBButton, NativeBase, Spinner} from 'native-base';
import {StyleSheet} from 'react-native';
import Theme from '../../../theme';
import {Paths} from '../../../Routes';
import {useHistory} from 'react-router-native';
import Text, {TextProps} from '../Text';

export interface ButtonProps extends NativeBase.Button {
  iconRightProps?: NativeBase.Icon;
  iconLeftProps?: NativeBase.Icon;
  loadingProps?: NativeBase.Spinner;
  showIconLeft?: boolean;
  showIconRight?: boolean;
  loading?: boolean;
  textProps?: Omit<TextProps, 'value'>;
  value: string;
  variant?: 'primary' | 'secondary';
  goTo?: Paths | '';
  goToData?: any;
  onPress?: any;
}

const Button: FunctionComponent<ButtonProps> = ({
  value = '',
  showIconLeft = false,
  showIconRight = false,
  iconRightProps = {},
  iconLeftProps = {},
  loadingProps = {},
  textProps = {},
  loading = false,
  style = {},
  variant = 'primary',
  goTo = '',
  goToData = {},
  onPress = undefined,
  ...props
}) => {
  const history = useHistory();

  const ShowLeftIcon = showIconLeft && (
    <Icon style={styles.icon} {...iconLeftProps} />
  );
  const ShowRightIcon = showIconRight && (
    <Icon style={styles.icon} {...iconRightProps} />
  );

  const ShowSpinner = loading && (
    <Spinner color="#fff" size="small" {...loadingProps} />
  );

  const handlePress = () => {
    if (goTo !== '') {
      history.push(goTo, goToData);
    }
    if (onPress) {
      onPress();
    }
  };

  return (
    <NBButton
      onPress={handlePress}
      style={{...styles.button, ...styles[variant], ...(style as any)}}
      disabled={loading || props.disabled}
      {...props}>
      {ShowLeftIcon}
      {ShowSpinner}
      <Text
        style={{
          ...styles.text,
          ...(variant === 'primary'
            ? styles.primaryText
            : styles.secondaryText),
        }}
        {...textProps}
        value={value}
      />
      {ShowRightIcon}
    </NBButton>
  );
};

const styles = StyleSheet.create({
  primary: {
    backgroundColor: Theme.colors.primary,
  },
  secondary: {
    backgroundColor: Theme.colors.secondary,
  },

  primaryText: {
    color: Theme.colors.white,
  },
  secondaryText: {
    color: Theme.colors.white,
  },

  button: {
    borderRadius: 10,
    justifyContent: 'center',
    minWidth: 100,
  },
  text: {
    textTransform: 'uppercase',
    color: '#fff',
  },
  icon: {
    color: Theme.colors.black,
  },
});

export default Button;

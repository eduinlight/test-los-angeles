import React from 'react';
import {ViewStyle} from 'react-native';
import Theme from '../../../theme';
import * as Animatable from 'react-native-animatable';

export interface SkeletonProps extends ViewStyle {}

const Skeleton: React.FC<SkeletonProps> = (props) => {
  const animation = {
    0: {opacity: 0.2, scale: 1},
    0.5: {opacity: 0.5, scale: 1.01},
    1: {opacity: 0.2, scale: 1},
  };

  return (
    <Animatable.View
      animation={animation}
      duration={3000}
      easing="ease-out"
      iterationCount="infinite"
      style={{
        backgroundColor: Theme.colors.grey0,
        ...props,
      }}
    />
  );
};

export default Skeleton;

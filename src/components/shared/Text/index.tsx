import React, {FunctionComponent} from 'react';
import {Text as NBText, NativeBase} from 'native-base';
import Theme from '../../../theme';
import {TextStyle} from 'react-native';

export interface TextProps extends NativeBase.Text {
  value: string;
  align?: TextStyle['textAlign'];
  color?: TextStyle['color'];
  size?: TextStyle['fontSize'];
  weight?: TextStyle['fontWeight'];
  lineHeight?: TextStyle['lineHeight'];
  style?: NativeBase.Text['style'];
}

const Text: FunctionComponent<TextProps> = ({
  value = '',
  align = 'left',
  color = Theme.colors.white,
  size = 16,
  style = {},
  weight = 'normal',
  lineHeight = 1.5,
  ...props
}) => {
  return (
    <NBText
      style={{
        textAlign: align,
        fontSize: size,
        fontWeight: weight,
        lineHeight: size * lineHeight,
        color,
        ...(style as any),
      }}
      {...props}>
      {value}
    </NBText>
  );
};

export default Text;

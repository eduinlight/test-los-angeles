import React, {FunctionComponent} from 'react';
import Theme from '../../../theme';
import Text, {TextProps} from '../Text';
import {Linking} from 'react-native';
import {useHistory} from 'react-router-native';
import NotiService from '../../../core/services/noti.service';
import useTranslate from '../../../core/hooks/translate/useTranslate';

export interface LinkTextProps extends TextProps {
  app?: boolean;
  to: string;
}

const LinkText: FunctionComponent<LinkTextProps> = ({
  to,
  app = true,
  ...props
}) => {
  const history = useHistory();
  const {translate} = useTranslate();

  const openTo = () => {
    if (app) {
      history.push(to);
    } else {
      Linking.openURL(to).catch(() => {
        NotiService.danger(translate('impossibleToOpenUrl'));
      });
    }
  };

  return <Text onPress={openTo} color={Theme.colors.white} {...props} />;
};

export default LinkText;

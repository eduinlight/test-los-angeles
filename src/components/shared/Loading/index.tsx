import React, {FunctionComponent} from 'react';
import {View, StyleSheet} from 'react-native';
import {Spinner} from 'native-base';

export interface LoadingProps {}

const Loading: FunctionComponent<LoadingProps> = (props: LoadingProps) => {
  const {} = props;
  return (
    <View style={styles.container}>
      <Spinner />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Loading;

import React from 'react';
import {Icon, Item, Input as NBInput, NativeBase, Label} from 'native-base';
import {StyleSheet} from 'react-native';
import Theme from '../../../theme';
import Column from '../Column';
import Text from '../Text';

export interface InputProps extends NativeBase.Input {
  iconRightProps?: NativeBase.Icon;
  iconLeftProps?: NativeBase.Icon;
  showIconLeft?: boolean;
  showIconRight?: boolean;
  itemProps?: NativeBase.Item;
  label?: string;
  required?: boolean;
  labelProps?: NativeBase.Label;
  success?: boolean;
  error?: boolean;
  errorText?: string;
}

const Input: React.FC<InputProps> = ({
  showIconLeft = false,
  showIconRight = false,
  iconRightProps = {},
  iconLeftProps = {},
  itemProps = {},
  label = '',
  labelProps = {},
  required = false,
  success = false,
  error = false,
  errorText = '',
  ...props
}) => {
  const renderRightIcon = showIconRight ? (
    <Icon style={styles.inputIcon} {...iconRightProps} />
  ) : (
    <></>
  );

  const renderLeftIcon = showIconLeft ? (
    <Icon style={styles.inputIcon} {...iconLeftProps} />
  ) : (
    <></>
  );

  const renderErrorIcon = error ? (
    <Icon {...Theme.icons.plus} style={{transform: [{rotate: '45deg'}]}} />
  ) : (
    <></>
  );

  const renderSuccessIcon = success ? <Icon {...Theme.icons.success} /> : <></>;

  return (
    <Column flex={0}>
      <Item success={success} error={error} {...itemProps}>
        <Label {...labelProps} style={{...styles.label}}>
          {label} {required && '*'}
        </Label>
        {renderLeftIcon}
        <NBInput
          style={styles.inputText}
          placeholderTextColor={Theme.colors.lightBlack}
          {...props}
        />
        {renderRightIcon}
        {renderErrorIcon}
        {renderSuccessIcon}
      </Item>
      {error && <Text size={14} color={Theme.colors.error} value={errorText} />}
    </Column>
  );
};

const styles = StyleSheet.create({
  input: {
    color: Theme.colors.white,
  },
  inputIcon: {
    color: Theme.colors.white,
  },
  inputText: {
    color: Theme.colors.white,
  },
  label: {
    color: Theme.colors.white,
  },
});

export default Input;

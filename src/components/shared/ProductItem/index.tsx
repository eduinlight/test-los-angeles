import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  TouchableOpacityProps,
} from 'react-native';
import Theme from '../../../theme';
import Product from '../../../core/interfases/product';
import Collumn from '../Column';
import Row from '../Row';
import Text, {TextProps} from '../Text';

export interface ProductItemProps extends TouchableOpacityProps {
  product: Product;
  productNameTextProps?: TextProps;
  productDepartmentProps?: TextProps;
  productCostProps?: TextProps;
}

const ProductItem: React.FC<ProductItemProps> = ({
  product: {name, cost, category, department},
  productNameTextProps = {},
  productCostProps = {},
  productDepartmentProps = {},
  ...props
}) => {
  return (
    <TouchableOpacity activeOpacity={0.7} {...props}>
      <Row flex={0} style={styles.container}>
        <Collumn flex={1}>
          <Text size={18} value={name} {...productNameTextProps} />
          <Text
            size={14}
            value={`${category} | ${department}`}
            {...productDepartmentProps}
          />
        </Collumn>
        <Collumn justifyContent="center" alignItems="flex-end" flex={1}>
          <Text
            size={18}
            weight={'bold'}
            value={`$ ${cost.toFixed(2)}`}
            {...productCostProps}
          />
        </Collumn>
      </Row>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Theme.colors.background,
    padding: Theme.spacing(1),
    height: 70,
    borderBottomColor: Theme.colors.border,
    borderBottomWidth: 1,
  },
});

export default ProductItem;

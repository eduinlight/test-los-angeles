import React, {FunctionComponent, useEffect} from 'react';
import useAppState from '../../../core/hooks/useAppState';

export interface AppStateProps {
  onBackground?: () => void;
  onActive?: () => void;
}

const AppState: FunctionComponent<AppStateProps> = ({
  onBackground,
  onActive,
}) => {
  const state = useAppState();

  useEffect(() => {
    if (state === 'active' && onActive) {
      onActive();
    } else if (state === 'background' && onBackground) {
      onBackground();
    }
  }, [onActive, onBackground, state]);

  return <></>;
};

export default AppState;

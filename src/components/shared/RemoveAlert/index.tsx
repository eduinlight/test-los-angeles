import React, {FunctionComponent} from 'react';
import {Body, Card, CardItem, View, NativeBase} from 'native-base';
import {StyleSheet, Modal} from 'react-native';
import Theme from '../../../theme';
import Text, {TextProps} from '../Text';
import Button, {ButtonProps} from '../Button';
import DimensionsService from '../../../core/services/dimensions.service';
import * as Animatable from 'react-native-animatable';
import useTranslate from '../../../core/hooks/translate/useTranslate';

const width = DimensionsService.windowWidth;

export interface RemoveAlertProps extends NativeBase.Button {
  title: string;
  message: string;
  open?: boolean;
  titleProps?: TextProps;
  messageProps?: TextProps;
  cancelButtonProps?: ButtonProps;
  okButtonProps?: ButtonProps;
  backgroundProps?: NativeBase.View;
  containerProps?: NativeBase.View;
  onOk?: () => void;
  onCancel?: () => void;
  loading?: boolean;
}

const RemoveAlert: FunctionComponent<RemoveAlertProps> = ({
  open = false,
  loading = false,
  title = '',
  message = '',
  titleProps = {},
  messageProps = {},
  cancelButtonProps = {},
  okButtonProps = {},
  backgroundProps = {},
  containerProps = {},
  onOk,
  onCancel,
}) => {
  const {translate} = useTranslate();

  return (
    <Modal
      animationType="fade"
      visible={open}
      presentationStyle="fullScreen"
      transparent>
      <Animatable.View style={styles.flex1} animation="fadeIn" duration={300}>
        <View
          style={[styles.flex1, styles.background, backgroundProps.style]}
          {...backgroundProps}>
          <Card style={[styles.card, containerProps.style]} {...containerProps}>
            <CardItem header style={styles.cardHeader}>
              <Text value={title} size={18} {...titleProps} />
            </CardItem>
            <CardItem>
              <Body>
                <Text value={message} {...messageProps} />
              </Body>
            </CardItem>
            <CardItem footer style={styles.cardFooter}>
              <Button
                onPress={onCancel}
                value={translate('cancelBtn')}
                style={styles.cancelBtn}
                textProps={{style: {color: '#000'}}}
                {...cancelButtonProps}
              />
              <Button
                style={styles.okBtn}
                loading={loading}
                onPress={onOk}
                value={translate('okBtn')}
                {...okButtonProps}
              />
            </CardItem>
          </Card>
        </View>
      </Animatable.View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  background: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52,52,52,0.5)',
  },
  card: {
    width: width * 0.8,
  },
  cardFooter: {
    borderTopColor: Theme.colors.black,
    borderTopWidth: 1,
    justifyContent: 'flex-end',
  },
  cardHeader: {
    borderBottomColor: Theme.colors.black,
    borderBottomWidth: 1,
    alignItems: 'flex-end',
  },
  cancelBtn: {
    backgroundColor: 'transparent',
    marginRight: 10,
    borderWidth: 0,
  },
  okBtn: {
    paddingHorizontal: 10,
  },
});

export default RemoveAlert;

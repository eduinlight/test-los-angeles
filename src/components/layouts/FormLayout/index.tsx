import React from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Icon,
  Button,
  Title,
  NativeBase,
} from 'native-base';
import {StyleSheet} from 'react-native';
import Theme from '../../../theme';
import {useHistory} from 'react-router-native';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';

export interface FormLayoutProps {
  title?: string;
  titleProps?: NativeBase.Title;
  onBackPress?: undefined | (() => void);
}

const FormLayout: React.FC<React.PropsWithChildren<FormLayoutProps>> = ({
  children,
  titleProps = {},
  title = '',
  onBackPress = undefined,
}) => {
  const history = useHistory();

  const goBack = () => {
    if (onBackPress) {
      onBackPress();
    } else {
      history.goBack();
    }
  };

  return (
    <Container style={styles.container}>
      <Header>
        <Left>
          <Button transparent onPress={goBack}>
            <Icon {...Theme.icons.backIcon} style={styles.backIcon} />
          </Button>
        </Left>
        <Body>
          <Title {...titleProps}>{title}</Title>
        </Body>
      </Header>
      <KeyboardAwareScrollView
        enableOnAndroid
        contentContainerStyle={styles.content}
        style={styles.content}>
        {children}
      </KeyboardAwareScrollView>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {backgroundColor: Theme.colors.black},
  content: {
    flex: 1,
  },
  backIcon: {
    color: Theme.colors.white,
    fontSize: 30,
  },
});

export default FormLayout;

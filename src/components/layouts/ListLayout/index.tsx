import React from 'react';
import {Container, Header, Body, Title} from 'native-base';
import {StyleSheet, ScrollView} from 'react-native';
import Theme from '../../../theme';
import Button from '../../shared/Button';
import Column from '../../shared/Column';
import Text from '../../shared/Text';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import useTranslate from '../../../core/hooks/translate/useTranslate';

export interface ListLayoutProps {
  onAdd: () => void;
  data: any[] | null;
  dataComponent: Element;
  loadingComponent: Element;
  title: string;
  noItemsText?: string;
  selectionTitle?: string;
  loading?: boolean;
}

const ListLayout: React.FC<ListLayoutProps> = (props: ListLayoutProps) => {
  const {translate} = useTranslate();
  const {
    onAdd,
    data,
    dataComponent,
    title,
    noItemsText = translate('noItems'),
    loadingComponent,
    loading = false,
  } = props;
  const renderLoading = loading && loadingComponent;

  const renderItems = !loading && data && data.length > 0 && (
    <ScrollView>{dataComponent}</ScrollView>
  );

  const renderAddBtn = !loading && data && data.length > 0 && (
    <Button
      variant="secondary"
      value="+"
      textProps={{size: Theme.spacing(4)}}
      style={{...styles.addButton, ...styles.floatBtn}}
      onPress={onAdd}
    />
  );

  const renderNoItems = !loading && data && data.length === 0 && (
    <Column
      spacing={Theme.spacing(2)}
      flex={1}
      alignItems="center"
      justifyContent="center">
      <Text value={noItemsText} align="center" style={styles.noItemsText} />
      <Text value={Theme.textIcons.fingerDown} />
      <Button
        variant="secondary"
        value="+"
        textProps={{size: Theme.spacing(4)}}
        style={styles.addButton}
        onPress={onAdd}
      />
    </Column>
  );

  return (
    <Container>
      <Header>
        <Body>
          <Title>{title}</Title>
        </Body>
      </Header>
      <KeyboardAwareScrollView
        enableOnAndroid
        contentContainerStyle={styles.content}
        style={styles.content}>
        {renderLoading}
        {renderItems}
        {renderNoItems}
        {renderAddBtn}
      </KeyboardAwareScrollView>
    </Container>
  );
};

const styles = StyleSheet.create({
  content: {flex: 1, backgroundColor: Theme.colors.background},
  addButton: {
    minWidth: Theme.spacing(7),
    height: Theme.spacing(7),
    borderRadius: 200,
    padding: 0,
  },
  floatBtn: {
    position: 'absolute',
    right: Theme.spacing(2),
    bottom: Theme.spacing(2),
  },
  noItemsText: {
    paddingHorizontal: Theme.spacing(3),
  },
});

export default ListLayout;

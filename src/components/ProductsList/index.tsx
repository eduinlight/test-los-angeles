import React from 'react';
import {Alert} from 'react-native';
import ListLayout from '../layouts/ListLayout';
import useTranslate from '../../core/hooks/translate/useTranslate';
import ProductItem from '../shared/ProductItem';
import ProductItemSkeleton from '../shared/ProductItemSkeleton';
import useGetProducts from '../../core/hooks/api/useGetProducts';
import NotiService from '../../core/services/noti.service';
import useOnMount from '../../core/hooks/useOnMount';
import {useDispatch} from 'react-redux';
import ProductsActions from '../../redux/products/actions';
import {useTypedSelector} from '../../core/hooks/use_typed_selector';
import useRoutes from '../../core/hooks/useRoutes';
import Product from '../../core/interfases/product';
import useUtilsForDev from '../../core/hooks/useUtilsForDev';
import {ActionSheet} from 'native-base';
import useDeleteProduct from '../../core/hooks/api/useDeleteProduct';

export interface ProductsListProps {}

var BUTTONS = [{text: 'Edit'}, {text: 'Delete'}, {text: 'Cancel'}];
var DESTRUCTIVE_INDEX = 1;
var CANCEL_INDEX = 2;

const ProductsList: React.FC<ProductsListProps> = (props) => {
  const {} = props;
  const {translate} = useTranslate();
  const products = useTypedSelector((store) => store.products.products);
  const {getProducts, loading, error, success} = useGetProducts();
  const dispatch = useDispatch();
  const {goToProductForm} = useRoutes();
  const {getDepartmentById, getCategoryById} = useUtilsForDev();
  const {deleteProduct} = useDeleteProduct();

  useOnMount(() => {
    if (!error && !success && products.length === 0) {
      getProducts()
        .then((_products) => {
          dispatch(ProductsActions.setProducts(_products));
        })
        .catch(() => {
          NotiService.danger(translate('getProductsError'));
        });
    }
  });

  const handleAddPress = () => {
    goToProductForm({type: 'add'});
  };

  const showDeleteProductAlert = (product: Product) => {
    Alert.alert(
      translate('deleteProductAlertTitle'),
      translate('deleteProductAlertMessage'),
      [
        {
          text: translate('cancel'),
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: translate('remove'),
          onPress: () => {
            deleteProduct(product.id || '')
              .then(() => {
                dispatch(ProductsActions.deleteProduct([product.id || '']));
                NotiService.success(translate('removeProductAlert'));
              })
              .catch(() => {
                NotiService.success(translate('removeProductAlertError'));
              });
          },
        },
      ],
      {cancelable: false},
    );
  };

  const handleProductPress = (product: Product) => () => {
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DESTRUCTIVE_INDEX,
        title: translate('actions'),
      },
      (buttonIndex) => {
        if (buttonIndex === 0) {
          dispatch(ProductsActions.setSelected(product));
          goToProductForm({type: 'edit'});
        } else if (buttonIndex === 1) {
          showDeleteProductAlert(product);
        }
      },
    );
  };

  return (
    <ListLayout
      data={products}
      loading={loading}
      noItemsText={translate('noProducts')}
      loadingComponent={<ProductItemSkeleton count={7} />}
      dataComponent={
        <>
          {products.map(({id, name, cost, category, department}) => (
            <ProductItem
              key={id}
              onPress={handleProductPress({
                id,
                name,
                cost,
                category,
                department,
              })}
              product={{
                id,
                name,
                cost,
                category: getCategoryById(category).name,
                department: getDepartmentById(department).name,
              }}
            />
          ))}
        </>
      }
      onAdd={handleAddPress}
      title={translate('products')}
      selectionTitle=""
    />
  );
};

export default ProductsList;

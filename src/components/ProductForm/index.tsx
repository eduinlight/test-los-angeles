import React, {useMemo, useEffect} from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import useTranslate from '../../core/hooks/translate/useTranslate';
import NotiService from '../../core/services/noti.service';
import useOnMount from '../../core/hooks/useOnMount';
import {useDispatch} from 'react-redux';
import ProductsActions from '../../redux/products/actions';
import Product from '../../core/interfases/product';
import Column from '../shared/Column';
import Input from '../shared/Input';
import Theme from '../../theme';
import Button from '../shared/Button';
import useControlledInput from '../../core/hooks/input/useControllerInput';
import Select, {SelectOption} from '../shared/Select';
import useGetDepartments from '../../core/hooks/api/useGetDepartments';
import useControlledSelect from '../../core/hooks/input/useControllerSelect';
import Category from '../../core/interfases/category';
import useAddProduct from '../../core/hooks/api/useAddProduct';
import Validator from '../../utils/validator';
import {RuleType} from '../../utils/validator/types';
import {useHistory} from 'react-router-native';
import useUpdateProduct from '../../core/hooks/api/useUpdateProduct';

export interface ProductFormProps {
  type?: 'add' | 'edit';
  selectedProduct?: Product | null;
}

const ProductForm: React.FC<ProductFormProps> = (props) => {
  const history = useHistory();
  const {type = 'add', selectedProduct = null} = props;
  const {translate} = useTranslate();
  const dispatch = useDispatch();

  const nameValidation: RuleType[] = ['required'];
  const name = useControlledInput({
    initialValue:
      (type === 'edit' && selectedProduct && selectedProduct.name) || undefined,
    validations: nameValidation,
  });

  const costValidation: RuleType[] = ['required', 'numeric'];
  const cost = useControlledInput({
    initialValue:
      (type === 'edit' && selectedProduct && selectedProduct.cost.toString()) ||
      undefined,
    validations: costValidation,
  });

  const department = useControlledSelect({
    initialValue:
      (type === 'edit' && selectedProduct && selectedProduct.department) ||
      undefined,
  });
  const category = useControlledSelect({
    initialValue:
      (type === 'edit' && selectedProduct && selectedProduct.category) ||
      undefined,
  });

  const model = {
    name: name.props.value,
    cost: parseFloat(cost.props.value),
    department: department.props.selectedValue,
    category: category.props.selectedValue,
  };

  const {
    getDepartments,
    data: departments,
    success: successLoadDepartments,
    error: errorLoadDepartments,
  } = useGetDepartments();

  const categories = useMemo<Category[]>(() => {
    if (departments && department.props.selectedValue) {
      for (const _department of departments) {
        if (_department.id === department.props.selectedValue) {
          return _department.categories;
        }
      }
    }
    return [];
  }, [departments, department.props.selectedValue]);
  const {addProduct, loading: loadingAdd} = useAddProduct();
  const {updateProduct, loading: loadingUpdate} = useUpdateProduct();

  const validForm = useMemo(() => {
    const _model = {
      name: name.props.value,
      cost: cost.props.value,
      department: department.props.selectedValue,
      category: category.props.selectedValue,
    };
    const validate = Validator.validate(_model, {
      name: nameValidation,
      cost: costValidation,
    });

    return validate.valid;
  }, [
    category.props.selectedValue,
    name.props.value,
    cost.props.value,
    department.props.selectedValue,
    costValidation,
    nameValidation,
  ]);

  const loading = loadingAdd || loadingUpdate;
  const addBtnText = loading ? translate('adding') : translate('add');
  const editBtnText = loading ? translate('editing') : translate('edit');
  const submitText = type === 'edit' ? editBtnText : addBtnText;

  useEffect(() => {
    if (department.props.selectedValue) {
      category.props.onValueChange(
        (categories.length > 0 && categories[0].id) || '',
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    categories,
    category.props.onValueChange,
    department.props.selectedValue,
  ]);

  useOnMount(() => {
    if (!errorLoadDepartments && !successLoadDepartments) {
      getDepartments().catch(() => {
        NotiService.danger(translate('getDepartmentsError'));
      });
    }
  });

  const handleAddProduct = () => {
    if (validForm) {
      addProduct(model)
        .then((product) => {
          dispatch(ProductsActions.addProduct(product));
          NotiService.success(translate('addProductSuccess'));
          history.goBack();
        })
        .catch(() => {
          NotiService.danger(translate('errorAdding'));
        });
    }
  };

  const handleUpdateProduct = () => {
    if (validForm && selectedProduct) {
      updateProduct(selectedProduct.id || '', model)
        .then((product) => {
          dispatch(ProductsActions.updateProduct(selectedProduct.id, product));
          NotiService.success(translate('updateProductSuccess'));
          history.goBack();
        })
        .catch(() => {
          NotiService.danger(translate('errorUpdating'));
        });
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.flexGrow}>
      <Column
        spacing={Theme.spacing(3)}
        justifyContent="space-between"
        style={styles.container}>
        <Column spacing={Theme.spacing(3)}>
          <Input
            autoFocus={type === 'add'}
            required
            itemProps={{floatingLabel: true}}
            label={translate('nameLabel')}
            keyboardType="default"
            returnKeyType="next"
            onSubmitEditing={() => cost.setFocus()}
            blurOnSubmit={true}
            {...name.props}
          />
          <Input
            required
            itemProps={{floatingLabel: true}}
            label={`${translate('costLabel')} ($)`}
            keyboardType="numeric"
            blurOnSubmit={true}
            {...cost.props}
          />
          <Select label={translate('departmentLabel')} {...department.props}>
            {departments &&
              departments.map(({id, name: _name}) => (
                <SelectOption key={id} label={_name} value={id} />
              ))}
          </Select>
          <Select label={translate('categoryLabel')} {...category.props}>
            {categories.map(({id, name: _name}) => (
              <SelectOption key={id} label={_name} value={id} />
            ))}
          </Select>
        </Column>

        <Button
          variant="secondary"
          disabled={loading || !validForm}
          loading={loading}
          value={submitText}
          onPress={type === 'add' ? handleAddProduct : handleUpdateProduct}
        />
      </Column>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: Theme.spacing(2),
    position: 'relative',
  },
  flexGrow: {
    flexGrow: 1,
  },
});

export default ProductForm;

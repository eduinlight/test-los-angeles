export enum ApiCallMethod {
  POST = 'post',
  PUT = 'put',
  DELETE = 'delete',
  GET = 'get',
}

export interface ApiCall {
  method: ApiCallMethod;
  url: string;
  data?: any;
  sendAtStart: boolean;
}

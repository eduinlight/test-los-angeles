import Category from './category';

export default interface Department {
  id: string;
  name: string;
  categories: Category[];
}

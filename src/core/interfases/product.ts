import Category from './category';
import Department from './department';

export default interface Product {
  id?: string;
  name: string;
  cost: number;
  category: Category['name'];
  department: Department['name'];
}

import {Toast} from 'native-base';

const duration = 3000;

class NotiServiceClass {
  info(text: string, buttonText: string = 'OK') {
    Toast.show({
      text,
      buttonText,
      duration,
    });
  }

  success(text: string, buttonText: string = 'OK') {
    Toast.show({
      text,
      buttonText,
      type: 'success',
      duration,
    });
  }

  danger(text: string, buttonText: string = 'OK') {
    Toast.show({
      text,
      buttonText,
      type: 'danger',
      duration,
    });
  }

  warning(text: string, buttonText: string = 'OK') {
    Toast.show({
      text,
      buttonText,
      type: 'warning',
      duration,
    });
  }
}

const NotiService = new NotiServiceClass();

export default NotiService;

import {Dimensions} from 'react-native';
class DimensionsServiceClass {
  screenWidth: number;
  screenHeight: number;
  windowWidth: number;
  windowHeight: number;
  constructor() {
    const screen = Dimensions.get('screen');
    const window = Dimensions.get('window');

    this.screenWidth = this.makeDivisibleByTwo(
      parseInt(screen.width.toFixed(0), 10),
    );
    this.screenHeight = this.makeDivisibleByTwo(
      parseInt(screen.height.toFixed(0), 10),
    );

    this.windowWidth = this.makeDivisibleByTwo(
      parseInt(window.width.toFixed(0), 10),
    );
    this.windowHeight = this.makeDivisibleByTwo(
      parseInt(window.width.toFixed(0), 10),
    );
  }

  private makeDivisibleByTwo(value: number): number {
    return value % 2 === 0 ? value : value + 1;
  }
}

const DimensionsService = new DimensionsServiceClass();

export default DimensionsService;

import {AxiosError} from 'axios';
import {useCallback} from 'react';
import useImmerState from './useImmerState';

export interface QueryState<T> {
  loading: boolean;
  success: boolean;
  data: null | T;
  error: null | AxiosError;
}

const useQueryState = <T>() => {
  const [state, setState] = useImmerState<QueryState<T>>({
    loading: false,
    success: false,
    error: null,
    data: null,
  });

  const setLoading = useCallback(() => {
    setState((newState) => {
      newState.loading = true;
      newState.error = null;
      newState.success = false;
    });
  }, [setState]);

  const setError = useCallback(
    (data: any) => {
      setState((newState) => {
        newState.loading = false;
        newState.error = data;
        newState.data = null;
      });
    },
    [setState],
  );

  const setSuccess = useCallback(
    (data: T) => {
      setState((newState) => {
        newState.loading = false;
        newState.success = true;
        newState.data = data;
      });
    },
    [setState],
  );

  return {
    state,
    setError,
    setSuccess,
    setLoading,
  };
};

export default useQueryState;

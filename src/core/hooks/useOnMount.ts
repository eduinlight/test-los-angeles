import {useEffect} from 'react';

function useOnMount(handler: () => void, unmountHandler = () => {}): void {
  useEffect(() => {
    handler();

    return unmountHandler();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
}

export default useOnMount;

// @ts-ignore
import db from '../../fake-api/db.json';
import {useCallback} from 'react';

const departmentsMap = db.departments.reduce(
  (acc: any, department: any) => ({...acc, [department.id]: department}),
  {},
);

const categoriesMap = db.departments.reduce(
  (acc: any, department: any) => ({
    ...acc,
    ...department.categories.reduce(
      (catAcc: any, category: any) => ({...catAcc, [category.id]: category}),
      {},
    ),
  }),
  {},
);

const useUtilsForDev = () => {
  const getDepartmentById = useCallback(
    (id: string) => departmentsMap[id] || {},
    [],
  );

  const getCategoryById = useCallback(
    (id: string) => categoriesMap[id] || {},
    [],
  );

  return {getDepartmentById, getCategoryById};
};

export default useUtilsForDev;

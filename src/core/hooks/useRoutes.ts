import {useHistory} from 'react-router-native';
import {useCallback} from 'react';
import {Paths} from '../../Routes';

const useRoutes = () => {
  const navigator = useHistory();

  const goTo = useCallback(
    (path: Paths, state: any = {}, replace: boolean = false) => {
      if (replace) {
        navigator.replace(path, state);
      } else {
        navigator.push(path, state);
      }
    },
    [navigator],
  );

  const goToProductsList = useCallback(
    (state: {}, replace: boolean = false) => {
      goTo(Paths.PRODUCTS_LIST, state, replace);
    },
    [goTo],
  );

  const goToProductForm = useCallback(
    (state: {type: 'edit' | 'add'}, replace: boolean = false) => {
      goTo(Paths.PRODUCT_FORM, state, replace);
    },
    [goTo],
  );

  return {goToProductsList, goToProductForm};
};

export default useRoutes;

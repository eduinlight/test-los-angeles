import {useCallback} from 'react';
import useQuery from '../useQuery';
import Config from '../../../config';
import {ApiCall} from '../../interfases/api';

const useDeleteProduct = () => {
  const state = useQuery<boolean>();

  const deleteProduct = useCallback(
    async (id: string): Promise<boolean> => {
      return (await state.sendRequest({
        url: `${Config.apiurl}/products/${id}`,
        method: 'delete',
        sendAtStart: false,
      } as ApiCall)) as boolean;
    },
    [state],
  );

  return Object.assign(state, {deleteProduct});
};

export default useDeleteProduct;

import {useCallback} from 'react';
import useQuery from '../useQuery';
import Product from '../../interfases/product';
import Config from '../../../config';
import {ApiCall} from '../../interfases/api';

const useGetProducts = () => {
  const state = useQuery<Product[]>();

  const getProducts = useCallback(async (): Promise<Product[]> => {
    return (await state.sendRequest({
      url: `${Config.apiurl}/products`,
      method: 'get',
      sendAtStart: false,
    } as ApiCall)) as Product[];
  }, [state]);

  return Object.assign(state, {getProducts});
};

export default useGetProducts;

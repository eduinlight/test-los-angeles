import {useCallback} from 'react';
import useQuery from '../useQuery';
import Product from '../../interfases/product';
import Config from '../../../config';
import {ApiCall} from '../../interfases/api';

const useUpdateProduct = () => {
  const state = useQuery<Product>();

  const updateProduct = useCallback(
    async (id: string, product: Product): Promise<Product> => {
      return (await state.sendRequest({
        url: `${Config.apiurl}/products/${id}`,
        method: 'put',
        data: product,
        sendAtStart: false,
      } as ApiCall)) as Product;
    },
    [state],
  );

  return Object.assign(state, {updateProduct});
};

export default useUpdateProduct;

import {useCallback} from 'react';
import useQuery from '../useQuery';
import Product from '../../interfases/product';
import Config from '../../../config';
import {ApiCall} from '../../interfases/api';

const useAddProduct = () => {
  const state = useQuery<Product>();

  const addProduct = useCallback(
    async (product: Product): Promise<Product> => {
      return (await state.sendRequest({
        url: `${Config.apiurl}/products`,
        method: 'post',
        data: product,
        sendAtStart: false,
      } as ApiCall)) as Product;
    },
    [state],
  );

  return Object.assign(state, {addProduct});
};

export default useAddProduct;

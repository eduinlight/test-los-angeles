import {useCallback} from 'react';
import useQuery from '../useQuery';
import Config from '../../../config';
import {ApiCall} from '../../interfases/api';
import Department from '../../interfases/department';

const useGetDepartments = () => {
  const state = useQuery<Department[]>();

  const getDepartments = useCallback(async (): Promise<Department[]> => {
    return (await state.sendRequest({
      url: `${Config.apiurl}/departments`,
      method: 'get',
      sendAtStart: false,
    } as ApiCall)) as Department[];
  }, [state]);

  return Object.assign(state, {getDepartments});
};

export default useGetDepartments;

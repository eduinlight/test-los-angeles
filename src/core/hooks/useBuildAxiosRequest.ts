import {useCallback} from 'react';
import {ApiCall, ApiCallMethod} from '../interfases/api';
import {AxiosRequestConfig} from 'axios';

const useBuildAxiosRequest = () => {
  // const {token} = useTypedSelector((store) => store.auth);

  const buildAxiosRequest = useCallback(
    (apiCall: ApiCall, options: AxiosRequestConfig = {}) => {
      const {method, data, url} = apiCall;
      const request: AxiosRequestConfig = {
        method,
        data,
        url,
        headers: {},
        ...options,
      };

      if (
        request.method === ApiCallMethod.DELETE ||
        request.method === ApiCallMethod.GET
      ) {
        delete request.data;
      }

      // if (token !== '') {
      // request.headers['x-access-token'] = token;
      // }

      return request;
    },
    [],
  );

  return {buildAxiosRequest};
};

export default useBuildAxiosRequest;

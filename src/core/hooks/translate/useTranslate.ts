import {useTypedSelector} from '../use_typed_selector';
import {useCallback} from 'react';
import translations from './locales';

export interface TranslationParam {
  replace: RegExp;
  text: string;
}

const useTranslate = () => {
  const language = useTypedSelector((store) => store.app.language);

  const translate = useCallback(
    (key: keyof typeof translations, params: TranslationParam[] = []) => {
      let text = translations[key][language];
      for (const param of params) {
        text = text.replace(param.replace, param.text);
      }
      return text;
    },
    [language],
  );

  return {
    translate,
  };
};

export default useTranslate;

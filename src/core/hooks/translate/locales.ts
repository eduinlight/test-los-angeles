export default {
  splashText: {
    en: 'This is an example',
    es: 'Esto es un ejemplo',
  },
  impossibleToOpenUrl: {
    en: 'Error open url',
    es: 'Ocurrió un error abriendo la url',
  },
  cancelBtn: {
    en: 'Cancel',
    es: 'Cancelar',
  },
  okBtn: {
    en: 'OK',
    es: 'Aceptar',
  },
  products: {
    en: 'Products',
    es: 'Productos',
  },
  noProducts: {
    en: 'There are no products to show. Add one here.',
    es: 'No existen productos para mostrar. Agrega uno aquí.',
  },
  getProductsError: {
    en: 'It was an error fetching the products.',
    es: 'Ocurrió un error cargando los productos',
  },
  editProductTitle: {
    en: 'Edit product',
    es: 'Editar producto',
  },
  addProductTitle: {
    en: 'Add product',
    es: 'Adicionar producto',
  },
  deleteProductAlertTitle: {
    en: 'Delete product',
    es: 'Eliminar producto',
  },
  deleteProductAlertMessage: {
    en: 'Are you sure of removing this product?',
    es: '¿Estás seguro de querer eliminar este producto?',
  },
  cancel: {
    en: 'Cancel',
    es: 'Cancelar',
  },
  remove: {
    en: 'Delete',
    es: 'Eliminar',
  },
  removeProductAlert: {
    en: 'The product was deleted successfully',
    es: 'El producto ha sido eliminado',
  },
  removeProductAlertError: {
    en: 'Delete product has fail',
    es: 'Ocurrió un error eliminando el producto',
  },
  actions: {
    en: 'Actions',
    es: 'Acciones',
  },
  noItems: {
    en: 'No items to show',
    es: 'No hay elementos para mostrar',
  },
  adding: {
    en: 'adding',
    es: 'edicionando',
  },
  add: {
    en: 'add',
    es: 'adicionar',
  },
  editing: {
    en: 'editing',
    es: 'editando',
  },
  edit: {
    en: 'edit',
    es: 'editar',
  },
  getDepartmentsError: {
    en: 'Error loading departments',
    es: 'Error cargando departamentos',
  },
  addProductSuccess: {
    en: 'The product was successfully added',
    es: 'El producto fue adicionado correctamente',
  },
  errorAdding: {
    en: 'Error adding',
    es: 'Error adicionando',
  },
  updateProductSuccess: {
    en: 'The product was successfully updated',
    es: 'El producto se actualizó correctamente',
  },
  errorUpdating: {
    en: 'Error updating',
    es: 'Error actualizando',
  },
  nameLabel: {
    en: 'Name',
    es: 'Nombre',
  },
  costLabel: {
    en: 'Cost',
    es: 'Costo',
  },
  departmentLabel: {
    en: 'Department',
    es: 'Departamento',
  },
  categoryLabel: {
    en: 'Category',
    es: 'Categoría',
  },
};

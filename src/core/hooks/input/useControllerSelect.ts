import {useCallback, useMemo} from 'react';
import {IObjectRuleType, RuleType} from '../../../utils/validator/types';
import useImmerState from '../useImmerState';

export interface UseControlledSelectParams {
  initialValue?: string;
  validations?: Array<RuleType | IObjectRuleType>;
}

const useControlledSelect = ({
  initialValue = '',
}: UseControlledSelectParams) => {
  const initialState = useMemo(
    () => ({
      selectedValue: initialValue,
    }),
    [initialValue],
  );
  const [{selectedValue}, setState] = useImmerState(initialState);

  const onValueChange = useCallback(
    (_value: string) => {
      setState((draft) => {
        draft.selectedValue = _value;
      });
    },
    [setState],
  );

  return {
    props: {
      selectedValue,
      onValueChange,
    },
  };
};

export default useControlledSelect;

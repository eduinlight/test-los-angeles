import {useCallback, useMemo, useRef} from 'react';
import Validator from '../../../utils/validator';
import {IObjectRuleType, RuleType} from '../../../utils/validator/types';
import useImmerState from '../useImmerState';

export interface UseControlledInputParams {
  initialValue?: string;
  validations?: Array<RuleType | IObjectRuleType>;
}

const useControlledInput = ({
  initialValue = '',
  validations = [],
}: UseControlledInputParams) => {
  const initialState = useMemo(
    () => ({
      value: initialValue,
      success: false,
      error: false,
      errorText: '',
      focus: false,
    }),
    [initialValue],
  );
  const [{value, errorText, error, focus, success}, setState] = useImmerState(
    initialState,
  );
  const inputRef = useRef<any>(null);

  const setFocus = useCallback(() => {
    if (inputRef.current) {
      inputRef.current._root.focus();
    }
  }, []);

  const clear = useCallback(() => {
    setState((draft) => {
      Object.assign(draft, initialState);
    });
  }, [initialState, setState]);

  const setError = useCallback(
    (_value: boolean, _errorText = '') => {
      setState((draft) => {
        draft.error = _value;
        draft.errorText = _errorText;
      });
    },
    [setState],
  );

  const setSuccess = useCallback(
    (_value: boolean) => {
      setState((draft) => {
        draft.success = _value;
      });
    },
    [setState],
  );

  const validate = useCallback(
    (form: {input: string}) => {
      const validation = Validator.validate(form, {input: validations});
      if (!validation.valid) {
        setError(true, validation.errors[0].error);
        setSuccess(false);
      } else {
        setError(false);
        setSuccess(true);
      }
      return validation.valid;
    },
    [setError, validations, setSuccess],
  );

  const onChangeText = useCallback(
    (_value: string) => {
      setState((draft) => {
        draft.value = _value;
      });
      validate({input: _value});
    },
    [setState, validate],
  );

  const onFocus = useCallback(() => {
    setState((draft) => {
      draft.focus = true;
    });
  }, [setState]);

  const onBlur = useCallback(() => {
    setState((draft) => {
      draft.focus = false;
    });
    validate({input: value});
  }, [setState, validate, value]);

  return {
    props: {
      value,
      onChangeText,
      onFocus,
      onBlur,
      success,
      error,
      errorText,
      getRef: (ref: any) => (inputRef.current = ref),
    },
    focus,
    setFocus,
    setError,
    clear,
    validate,
    ref: inputRef,
  };
};

export default useControlledInput;
